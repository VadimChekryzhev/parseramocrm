<?php

/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 14.10.2017
 * Time: 11:20
 */

namespace modules\base\controllers;

use airux\mvc\controllers\Controller;
use airuxamo\Api;
use airuxamo\Config;
use airux\console\Output;
use airuxamo\models\Lead;
use airuxamo\models\Task;
use function Sodium\add;

class Parser extends Controller {
	/**
	 * @var Output
	 */
	protected $output;

	public function getAccessRules() {
		return [
			"msg" => ["allow" => ["*"]],
			"post" => ["allow" => ["*"]],
			"getlead" => ["allow" => ["*"]],
		];
	}


	public function autorizedAction() {
		$this->output = new Output();
		$config = new Config();
		$config->login = $this->services->config->get("amo.login");
		$config->subDomain = $this->services->config->get("amo.subdomain");
		$config->apiKey = $this->services->config->get("amo.hash");
		$amoApi = new Api($config);
		if (!$amoApi->auth()) {
			throw new \Exception("Не удалось авторизоваться amo");
		} else {
			$this->output->writeln("Авторизаця прошла успешно");
		}
		return $amoApi;
	}


	public function getleadAction() {
		$this->output = new Output();
		$amoApi = $this->autorizedAction();
		$leadDataArray = $amoApi->getEntities("leads", null);
		$this->output->writeln($leadDataArray);
	}


	public function outFileAction($file, $msg) {
		$current = file_get_contents($file);
		$current .= $msg;
		file_put_contents($file, $current);
	}

	public function openImap($post) {
		$hostname = $this->services->config->get($post . ".hostname");
		$username = $this->services->config->get($post . ".username");
		$password = $this->services->config->get($post . ".password");


		$inbox = imap_open($hostname, $username, $password, OP_READONLY);
		if ($inbox) {
			$this->output->writeln("Connect successfully\n");
		} else {
			throw new \Exception('CONNECT ERROR: ' . imap_last_error() . "\n");
		}
		return $inbox;
	}

	public function getEmails($inbox) {
		$str = "ON \"" . date("j F Y") . "\"";
		$emails = imap_search($inbox, $str);
		if (!$emails) {
			$this->output->writeln("Inbox is empty");
			die();
		}
		return $emails;
	}

	public function getRegExp() {
		return [
			"/СЧЕТ\s*№\s*[0-9]+/i", // Alpha Bank
			"/№\s*ФТС\s*[0-9]+/i", // new Alpha Bank
			"/ФТС\s*[0-9]+/i", // new Alpha Bank
			"/По счету [0-9]+/i", // new Alpha Bank
			"/ПО ДОГОВОРУ ОФЕРТЫ №\s*[0-9]+/i", // new Alpha Bank
			"/ПО СЧЕТАМ №\s*ФТС\s*[0-9]+/i", // new Alpha Bank

			"/Идентификатор клиента: [0-9]+/i", // Yandex Money
			"/заказ №\s*[.]*[0-9]+/i", // Yandex Money
			"/№\s+[0-9]{7,}/i", // Yandex Money

			"/Счёт\s*[0-9]+/i", // Sbarbank
			"/Номер\s*[0-9]+/i", // Sbarbank
			"/Сообщение:\s*\"[0-9]+\"/i", // Sberbank
			"/Номер заказа\s*[-]*\s*[0-9]+/i", // Sberbank
			"/Сообщение: \"Номер\s*[0-9]+/i", // Sberbank
			"/Сообщение: \"Ном\s*[0-9]+/i", // Sberbank
			"/Сообщение: \".*аказ.*[0-9]+/i", // Sberbank
		];
	}

	public function getCustomerId($message) {
		$customer_id = null;
		foreach ($this->getRegExp() as $reg) {
			if (preg_match($reg, $message, $matches)) {
				preg_match('/[0-9]+/', $matches[0], $result);
				$customer_id = $result[0];
				break;
			} else {
				$customer_id = null;
			}
		}
		return $customer_id;
	}

	public function getAnswer($message, $customer_id) {
		$answer = "";
		$sum = "";
		$s_mess = "";
		if (preg_match('/Сбербанк Онлайн/i', $message, $result)) {
			if (preg_match('/[0-9]+[.]{1}[0-9]+\s(RUB)/', $message, $s_sum)) {
				$sum = ". Сумма " . $s_sum[0];
			}
			if (preg_match("/Сообщение: \".*\"/i", $message, $mess)) {
				$s_mess = ". " . $mess[0];
			}
			$answer = 'Подтвердить приход оплаты от Сбербанка ' . $customer_id . $sum . $s_mess;
		} else if (preg_match('/Альфа-Банк/i', $message, $result)) {
			if (preg_match('/сумму [0-9]+[,]{1}[0-9]+/', $message, $a_sum)) {
				$sum = ". На " . $a_sum[0];
			}
			$answer = 'Подтвердить приход оплаты от Альфа банка ' . $customer_id . $sum;
		} else if (preg_match('/Извещение № [0-9]+/i', $message, $result)) {
			if (preg_match('/Сумма:\s*[0-9]+[.]{1}[0-9]+\s*RUB/i', $message, $y_sum)) {
				$sum = ". " . $y_sum[0];
			}
			$answer = 'Подтвердить приход оплаты от Yandex Money ' . $customer_id . $sum;
		}
		return $answer;
	}

	protected function getLead($customerId, Api $amoApi) {
		$leadDataArray = $amoApi->getEntities("leads", [
			"query" => $customerId
		]);
		if (!$leadDataArray) {
			return null;
		}

		/**
		 * Поиск по полю в найденных сделках
		 */
		$isFound = false;
		$lead = null;
		foreach ($leadDataArray as $leadData) {
			$lead = new Lead($leadData);
			foreach ($lead->custom_fields as $customField) {
				if ($customField->name == "Номер сделки") {
					foreach ($customField->values as $value) {
						if ($value["value"] == $customerId) {
							$isFound = true;
							break 2;
						}
					}
				}
			}
		}
		if (!$isFound) {
			return null;
		}
		return $lead;

	}


	public function postAction() {
		$this->output = new Output();
		$post = "yandex";
		$section = $this->services->config->get($post . ".section");

		$inbox = $this->openImap($post);
		$emails = $this->getEmails($inbox);
		$regExp = $this->getRegExp();

		$amoApi = $this->autorizedAction();
		rsort($emails);


		foreach ($emails as $email_number) {
			$overview = imap_fetch_overview($inbox, $email_number, 0);
			$message = imap_base64(imap_fetchbody($inbox, $email_number, $section));
			if (!preg_match('/Сбербанк Онлайн/i', $message, $result) && !preg_match('/Альфа-Банк/i', $message, $result)) {
				$message = mb_convert_encoding($message, "UTF-8", "windows-1251");
			}

			$this->output->writeln(str_repeat('-', 10) . "MSG" . str_repeat('-', 10));
			$this->output->writeln($message);
			$this->output->writeln(str_repeat('-', 10) . "END" . str_repeat('-', 10));

			$customer_id = $this->getCustomerId($message);

			$fileName = DIR_ROOT . "ids-" . date("Y-m-d") . ".txt";
			is_file($fileName) or touch($fileName);

			$file_id = file($fileName, FILE_IGNORE_NEW_LINES);
			if (isset($customer_id) && in_array($customer_id, $file_id)) {
				$this->output->writeln("Skip id " . $customer_id);
				continue;
			} elseif (!(isset($customer_id) && !in_array($customer_id, $file_id))) {
				$this->output->writeln("В письме не найдено ID сделки");
				$this->output->writeln(str_repeat("*", 40));
				continue;
			}

			$this->output->writeln("QUERY: " . $customer_id);

			$lead = $this->getLead($customer_id, $amoApi);
			if (!$lead) {
				$this->output->writeln("Сделка не найдена с id " . $customer_id);
				continue;
			}

			$answer = $this->getAnswer($message, $customer_id);
			$task = new Task();

			$task->text = $answer;
			$task->element_type = Task::ELEMENT_LEAD;
			$task->element_id = $lead->id;
			$task->responsible_user_id = $lead->responsible_user_id;
			$task->date_create = time();
			$task->status = 0;

			// Раскоменитить для создания задачи
			if ($amoApi->setEntities($task)) {
				$this->output->writeln("OK");
			} else {
				$this->output->writeln("\nОшибка при создании новой задачи по сделке с ID" . $customer_id);
			}
			$this->outFileAction($fileName, $customer_id . "\n");
			$this->output->writeln("Task text: " . $answer);
			$this->output->writeln(str_repeat("*", 40));
		}
		imap_close($inbox);
		$this->output->writeln("Connect to mailbox is closed");
	}

}


//    $date = date("D, j M Y H:i:s", strToTime("-1 hour"));
