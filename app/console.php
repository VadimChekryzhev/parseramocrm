#!/usr/bin/env php
<?php

/**
 * Created by PhpStorm.
 * User: 1
 * Date: 04.04.2017
 * Time: 11:35
 */
defined("DS") or define("DS", DIRECTORY_SEPARATOR);
defined("DIR_ROOT") or define("DIR_ROOT", dirname(__DIR__) . DS);
defined("DIR_APP") or define("DIR_APP", DIR_ROOT . "app" . DS);

require DIR_ROOT . "/vendor/autoload.php";

$config = include dirname(__FILE__) . "/configs/main.php";

$script = new \airux\ConsoleScript($config);
$script->setArguments($argv);
$script->init();
$script->run();
