<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 23.03.2017
 * Time: 10:18
 */
/* @var $logger \airux\base\Log */

defined("DS") or define("DS", DIRECTORY_SEPARATOR);
defined("DIR_ROOT") or define("DIR_ROOT", dirname(__FILE__) . DS);
defined("DIR_APP") or define("DIR_APP", DIR_ROOT . "app" . DS);


require_once DIR_ROOT . "/vendor/autoload.php";
require_once DIR_ROOT . "/vendor/larexsetch/airuxphp/constants.php";

$config = include DIR_APP . "configs/main.php";

$requestUri = ($_SERVER['REQUEST_URI'] == '/') ? '/index.html' : $_SERVER['REQUEST_URI'];
$requestUriArray = explode('?', $requestUri);
$uri = $requestUriArray[0];

if (preg_match("/^\\/api\\/.*/i", $uri)) {
	$script = new \airux\ApiScript($config);
} else {
	$script = new \airux\WebScript($config);
}

try {
	$script->init();
	$script->run();
	$script->sendResponse();
} catch (Exception $e) {
	$script->processException($e);
	$script->sendResponse();
}

$client = new Google_Client();

